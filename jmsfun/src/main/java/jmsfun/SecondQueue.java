package jmsfun;



import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.naming.InitialContext;
import javax.naming.NamingException;


public class SecondQueue {

	public static void main(String[] args) throws Exception{
		InitialContext initialContext=null;
		Connection connection=null;
		
		
			initialContext=new InitialContext();
			ConnectionFactory cf=(ConnectionFactory) initialContext.lookup("ConnectionFactory");
			 connection=cf.createConnection();
		    Session session=connection.createSession();
		    Topic topic=(Topic) initialContext.lookup("topic/myTopic");
		    MessageProducer producer= session.createProducer(topic);
		    MessageConsumer messageConsumer1= session.createConsumer(topic);
		    MessageConsumer messageConsumer2= session.createConsumer(topic);
		    MessageConsumer messageConsumer3= session.createConsumer(topic);
		    TextMessage textMessage=session.createTextMessage("I am shivam singh and this is the first message");
		    producer.send(textMessage);
		    System.out.println("write"+" "+textMessage.getText());
		   
		    connection.start();
		    TextMessage messageReceived1=(TextMessage) messageConsumer1.receive(5000);
		    TextMessage messageReceived2=(TextMessage) messageConsumer2.receive(5000);
		    TextMessage messageReceived3=(TextMessage) messageConsumer3.receive(5000);
		    System.out.println("read"+" "+messageReceived1.getText());
		    System.out.println("read"+" "+messageReceived2.getText());
		    System.out.println("read"+" "+messageReceived3.getText());
		    
		 connection.close();
		 initialContext.close();
		
	
		
	}
	}


