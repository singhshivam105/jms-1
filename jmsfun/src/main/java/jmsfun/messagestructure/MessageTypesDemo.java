package jmsfun.messagestructure;

 import javax.jms.Queue;
import javax.jms.StreamMessage;
import javax.jms.TemporaryQueue;
import javax.jms.TextMessage;

import java.util.HashMap;

import javax.jms.BytesMessage;
import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.activemq.artemis.jms.client.ActiveMQConnectionFactory;

public class MessageTypesDemo {

	public static void main(String[] args) throws NamingException, JMSException, InterruptedException {
		
		InitialContext context=new InitialContext();
		Queue queue=(Queue) context.lookup("queue/myQueue");
		Queue expiryqueue=(Queue) context.lookup("queue/expiryQueue");
		//Queue replyQueue=(Queue) context.lookup("queue/replyQueue");
		//Queue queue2=(Queue) context.lookup("queue/replyQueue");
		
		try(ActiveMQConnectionFactory abc=new ActiveMQConnectionFactory();
		JMSContext jmscontext=abc.createContext()){
		JMSProducer producer = jmscontext.createProducer();
		BytesMessage bm=jmscontext.createBytesMessage();
		bm.writeUTF("john");
		bm.writeLong(65678);
		bm.setBooleanProperty("ab", true);
		
		
		
		
		StreamMessage stm=jmscontext.createStreamMessage();
		stm.writeBoolean(true);
		stm.writeFloat(4.5f);
	
		MapMessage mm=jmscontext.createMapMessage();
		mm.setBoolean("isCredit", true);
		
		
		ObjectMessage obm=jmscontext.createObjectMessage();
		Patient patient=new Patient();
		patient.setId(22);
		patient.setName("shivam");
		
		obm.setObject(patient);
		
		producer.send(queue, obm);
		
		
		//BytesMessage messageReceived=(BytesMessage) jmscontext.createConsumer(queue).receive();
		//System.out.println(messageReceived.readUTF());
		//System.out.println(messageReceived.readLong());
		//System.out.println(messageReceived.getBooleanProperty("ab"));
		
		//StreamMessage mrec=(StreamMessage) jmscontext.createConsumer(queue).receive(5000);
		//System.out.println(mrec.readBoolean());
		//System.out.println(mrec.readFloat());
		
		
		//MapMessage mrec=(MapMessage) jmscontext.createConsumer(queue).receive(5000);
		//System.out.println(mrec.getBoolean("isCredit"));
	
		//ObjectMessage oo=(ObjectMessage) jmscontext.createConsumer(queue).receive(5000);
		Patient cpm=jmscontext.createConsumer(queue).receiveBody(Patient.class);
		//Patient cpm=(Patient) oo.getObject();
		System.out.println(cpm.getId()+""+cpm.getName());
		}
	}}
	