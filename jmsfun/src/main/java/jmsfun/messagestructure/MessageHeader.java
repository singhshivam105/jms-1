package jmsfun.messagestructure;

import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.Message;
import javax.jms.Queue;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.activemq.artemis.jms.client.ActiveMQConnectionFactory;

public class MessageHeader {

	public static void main(String[] args) throws NamingException {
		InitialContext context=new InitialContext();
		Queue queue=(Queue) context.lookup("queue/myQueue");
		
		try(ActiveMQConnectionFactory amqc=new ActiveMQConnectionFactory();
				JMSContext jmscontext=amqc.createContext()){
			   JMSProducer jmpsp=jmscontext.createProducer();
			   
			   String[] ab=new String[3];
			   ab[0]="First message";
			   ab[1]="Second message";
			   ab[2]="Third message";
			   
			  // jmpsp.setPriority(3);
			   jmpsp.send(queue, ab[0]);
			   
			   //jmpsp.setPriority(1);
			   jmpsp.send(queue, ab[1]);
			   
			   
			   //jmpsp.setPriority(9);
			   jmpsp.send(queue, ab[2]);
			   
			   JMSConsumer jmsc=jmscontext.createConsumer(queue);
			   
			   for(int i=0;i<ab.length;i++) {
				   //System.out.println(jmsc.receiveBody(String.class));
				   Message message=jmsc.receive();
				   try {
					System.out.println(message.getJMSPriority());
				} catch (JMSException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			   }
			   
			   
		}
	}

}
