package jmsfun.messagestructure;

 import javax.jms.Queue;
import javax.jms.TemporaryQueue;
import javax.jms.TextMessage;

import java.util.HashMap;

import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.activemq.artemis.jms.client.ActiveMQConnectionFactory;

public class RequestReplyDemo {

	public static void main(String[] args) throws NamingException, JMSException {
		
		InitialContext context=new InitialContext();
		Queue queue=(Queue) context.lookup("queue/myQueue");
		//Queue replyQueue=(Queue) context.lookup("queue/replyQueue");
		//Queue queue2=(Queue) context.lookup("queue/replyQueue");
		
		try(ActiveMQConnectionFactory abc=new ActiveMQConnectionFactory();
				JMSContext jmscontext=abc.createContext()){
			
			JMSProducer producer = jmscontext.createProducer();
			TemporaryQueue replyQueue=jmscontext.createTemporaryQueue();
			TextMessage tm=jmscontext.createTextMessage("arise awake and not");
			try {
				tm.setJMSReplyTo(replyQueue);
			} catch (JMSException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			producer.send(queue, tm);
			try {
				System.out.println(tm.getJMSMessageID());
			} catch (JMSException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			HashMap<String, TextMessage> rqm=new HashMap<>();
			rqm.put(tm.getJMSMessageID(), tm);
			
			JMSConsumer createConsumer = jmscontext.createConsumer(queue);
			TextMessage message=(TextMessage) createConsumer.receive();
			try {
				System.out.println(message.getText());
			} catch (JMSException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			JMSProducer reply=jmscontext.createProducer();
			try {
			
				TextMessage tm1=jmscontext.createTextMessage("arise awake and not");
				reply.setJMSCorrelationID(message.getJMSMessageID());
				reply.send(message.getJMSReplyTo(), tm1);
				
			} catch (JMSException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			JMSConsumer replyConsumer = jmscontext.createConsumer(replyQueue);
			//String message1=replyConsumer.receiveBody(String.class);
			//System.out.println(message1);
			TextMessage mt2=(TextMessage) replyConsumer.receive();
			System.out.println(mt2.getJMSCorrelationID());
			System.out.println(rqm.get(mt2.getJMSCorrelationID()).getText());
					
			
		}
		
	}

}
