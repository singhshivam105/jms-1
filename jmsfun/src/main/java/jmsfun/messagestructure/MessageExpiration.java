package jmsfun.messagestructure;

 import javax.jms.Queue;
import javax.jms.TemporaryQueue;
import javax.jms.TextMessage;

import java.util.HashMap;

import javax.jms.JMSConsumer;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.JMSProducer;
import javax.jms.Message;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.activemq.artemis.jms.client.ActiveMQConnectionFactory;

public class MessageExpiration {

	public static void main(String[] args) throws NamingException, JMSException, InterruptedException {
		
		InitialContext context=new InitialContext();
		Queue queue=(Queue) context.lookup("queue/myQueue");
		Queue expiryqueue=(Queue) context.lookup("queue/expiryQueue");
		//Queue replyQueue=(Queue) context.lookup("queue/replyQueue");
		//Queue queue2=(Queue) context.lookup("queue/replyQueue");
		
		try(ActiveMQConnectionFactory abc=new ActiveMQConnectionFactory();
		JMSContext jmscontext=abc.createContext()){
		JMSProducer producer = jmscontext.createProducer();
		TextMessage tm=jmscontext.createTextMessage("arise awake and not");
		producer.setTimeToLive(2000);
		producer.send(queue, tm);
		Thread.sleep(5000);
		Message messageReceived=jmscontext.createConsumer(queue).receive(5000);
		System.out.println(messageReceived);
		System.out.println(jmscontext.createConsumer(expiryqueue));
		}
	}}
	