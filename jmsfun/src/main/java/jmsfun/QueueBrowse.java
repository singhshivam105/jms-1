package jmsfun;



import java.util.Enumeration;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.InitialContext;
import javax.naming.NamingException;


public class QueueBrowse {

	public static void main(String[] args) {
		InitialContext initialContext=null;
		Connection connection=null;
		
		try {
			initialContext=new InitialContext();
			ConnectionFactory cf=(ConnectionFactory) initialContext.lookup("ConnectionFactory");
			 connection=cf.createConnection();
		    Session session=connection.createSession();
		    Queue queue=(Queue) initialContext.lookup("queue/myQueue");
		    MessageProducer producer= session.createProducer(queue);
		    TextMessage textMessage1=session.createTextMessage("I am shivam singh and this is the first message");
		    TextMessage textMessage2=session.createTextMessage("I am lal and this is the first message");
		    TextMessage textMessage3=session.createTextMessage("I am john singh and this is the first message");
		    producer.send(textMessage1);
		    producer.send(textMessage2);
		    producer.send(textMessage3);
		    QueueBrowser browser= session.createBrowser(queue);
		    Enumeration messagesEnum=browser.getEnumeration();
		    while(messagesEnum.hasMoreElements()) {
		    	TextMessage textMessage=(TextMessage) messagesEnum.nextElement();
		    	System.out.println(textMessage.getText());
		    }
		 
		    MessageConsumer messageConsumer= session.createConsumer(queue);
		    connection.start();
		    TextMessage messageReceived=(TextMessage) messageConsumer.receive(5000);
		    System.out.println("read"+" "+messageReceived.getText());
		    messageReceived=(TextMessage) messageConsumer.receive(5000);
		    System.out.println("read"+" "+messageReceived.getText());
		    messageReceived=(TextMessage) messageConsumer.receive(5000);
		    System.out.println("read"+" "+messageReceived.getText());
		    
			} 
		 catch (NamingException e) {
				e.printStackTrace();
}
		catch (JMSException e) {
				
				e.printStackTrace();
			}
	
		finally{
			if(initialContext!=null) {
				try {
					initialContext.close();
				} catch (NamingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
					
				}
			if(connection!=null) {
				try {
					connection.close();
				} catch (JMSException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	}


